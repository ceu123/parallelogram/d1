var form = document.getElementById("areaParallel");

form.onsubmit = function() {
    var base = parseFloat(form.base.value),
        height = parseFloat(form.height.value),
        answer = base * height;

        form.answer.value = answer;

        return false;
};